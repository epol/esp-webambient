# esp webambient

This program (to be installed on a ESP8266) reads every 30 seconds temperature
and humidity data from a DHT22 sensor and shows them in a webpage updating it
via websocket.

This work is based on the examples of the
[esp-open-rtos](https://github.com/SuperHouse/esp-open-rtos) project.

## How to build

You should have esp SDK installed (I used
[esp-open-sdk](https://github.com/pfalcon/esp-open-sdk)) with the proper values
exported in PATH and a copy of
[esp-open-rtos](https://github.com/SuperHouse/esp-open-rtos) in the upper
directory of this project

```
some_dir
+-- esp-open-rtos
|   +-- ...
|   +-- ...
|   +-- common.mk
|   +-- ...
+-- webambient         <-- this project
    +-- Makefile
    +-- main.c
    +-- ...
```

This are the steps to build the project:

 * `make html` generates the file _fsdata.c_ with the html files in C format
 * `make` builds the project
 * `make flash` loads the project in the MCU

To monitor the serial output the command `miniterm /dev/ttyUSB0 115200` can be
used.