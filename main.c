/*
 *  esp-webambient - main.c
 *  Copyright (C) 2018  Enrico Polesel
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* ESP libraries */
#include <espressif/esp_common.h>
#include <esp8266.h>
#include <esp/uart.h>

/* C libraries */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* FreeRTOS libraries */
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

/* lwIP libraries */
#include <lwip/err.h>
#include <lwip/sockets.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/dns.h>

/* Components libraries */
#include <httpd/httpd.h>
#include <dht/dht.h>
#include <sntp.h>

/* Configurations */
#include <ssid_config.h>



/* Hard coded values */
uint8_t const dht_gpio = 3;
const dht_sensor_type_t sensor_type = DHT_TYPE_DHT22;

#define SNTP_SERVERS 	"0.pool.ntp.org", "1.pool.ntp.org", \
						"2.pool.ntp.org", "3.pool.ntp.org"


/* Custom Macro */
#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)
#define UNUSED_ARG(x)	(void)x

/* Global variables */
struct ambient_t {
    int16_t temperature;
    int16_t humidity;
    time_t last_updated;
} ambient;

/*struct common_t {
    struct ambient_t ambient;
    SemaphoreHandle_t ambientSemaphore;
    };*/

SemaphoreHandle_t ambientSemaphore;



/* CODE */

/* Wait for IP
 * 
 * Wait until a IP is received from DHCP
 */
void waitForIP()
{
    while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP) {
        vTaskDelayMs(200);
    }
}


/* DHT Measurement Task
 *
 * this task will poll the DHT sensor periodically and save the
 * obtained measures in the ambient global variable
 */
void dhtMeasurementTask(void *pvParameters)
{
    int16_t temperature = 0;
    int16_t humidity = 0;
    
    /* DHT sensors that come mounted on a PCB generally have
     * pull-up resistors on the data pin.  It is recommended
     * to provide an external pull-up resistor otherwise...
     */
    gpio_set_pullup(dht_gpio, false, false);

    while(1) {
        if (dht_read_data(sensor_type, dht_gpio, &humidity, &temperature)) {
            /*printf("Humidity: %f%% Temp: %fC\n", 
                   (float)humidity / 10, 
                   (float)temperature / 10);*/
            time_t cur_time = time(NULL);
            if (xSemaphoreTake(ambientSemaphore,portTICK_PERIOD_MS*1000) == pdTRUE) {
                ambient.temperature = temperature;
                ambient.humidity = humidity;
                ambient.last_updated = cur_time;
                xSemaphoreGive(ambientSemaphore);
            }
            else {
                printf("Unable to take semaphore to save data\n");
            }
            
        } else {
            printf("Could not read data from sensor\n");
        }

        vTaskDelayMs(30*1000);
    }
}


/* Custom enum for SSI handling */ 
enum {
    SSI_UPTIME,
    SSI_FREE_HEAP,
    SSI_TEMP,
    SSI_HUMIDITY,
    SSI_AMB_UPDATED,
};

int32_t ssi_handler(int32_t iIndex, char *pcInsert, int32_t iInsertLen)
{
    switch (iIndex) {
    case SSI_UPTIME:
        snprintf(pcInsert, iInsertLen, "%d",
                 xTaskGetTickCount() * portTICK_PERIOD_MS / 1000);
        break;
    case SSI_FREE_HEAP:
        snprintf(pcInsert, iInsertLen, "%d", (int) xPortGetFreeHeapSize());
        break;
    case SSI_TEMP:
        if (xSemaphoreTake(ambientSemaphore,portTICK_PERIOD_MS*1000*2) == pdTRUE) {
            int16_t temperature = ambient.temperature;
            xSemaphoreGive(ambientSemaphore);
            snprintf(pcInsert, iInsertLen, "%.2f", (float)temperature/10);
        }
        else {
            printf("Unable to get ambient mutex\n");
            snprintf(pcInsert, iInsertLen, "N/A");
        }
        break;
    case SSI_HUMIDITY:
        if (xSemaphoreTake(ambientSemaphore,portTICK_PERIOD_MS*1000*2) == pdTRUE) {
            int16_t humidity = ambient.humidity;
            xSemaphoreGive(ambientSemaphore);
            snprintf(pcInsert, iInsertLen, "%.2f", (float)humidity/10);
        }
        else {
            printf("Unable to get ambient mutex\n");
            snprintf(pcInsert, iInsertLen, "N/A");
        }
        break;
    case SSI_AMB_UPDATED:
        if (xSemaphoreTake(ambientSemaphore,portTICK_PERIOD_MS*1000*2) == pdTRUE) {
            time_t last_updated = ambient.last_updated;
            xSemaphoreGive(ambientSemaphore);
            snprintf(pcInsert, iInsertLen, "%s", ctime(&last_updated));
        }
        else {
            printf("Unable to get ambient mutex\n");
            snprintf(pcInsert, iInsertLen, "N/A");
        }
        break;

    default:
        snprintf(pcInsert, iInsertLen, "N/A");
        break;
    }

    /* Tell the server how many characters to insert */
    return (strlen(pcInsert));
}

/*char *gpio_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
    for (int i = 0; i < iNumParams; i++) {
        if (strcmp(pcParam[i], "on") == 0) {
            uint8_t gpio_num = atoi(pcValue[i]);
            gpio_enable(gpio_num, GPIO_OUTPUT);
            gpio_write(gpio_num, true);
        } else if (strcmp(pcParam[i], "off") == 0) {
            uint8_t gpio_num = atoi(pcValue[i]);
            gpio_enable(gpio_num, GPIO_OUTPUT);
            gpio_write(gpio_num, false);
        } else if (strcmp(pcParam[i], "toggle") == 0) {
            uint8_t gpio_num = atoi(pcValue[i]);
            gpio_enable(gpio_num, GPIO_OUTPUT);
            gpio_toggle(gpio_num);
        }
    }
    return "/index.ssi";
    }*/

char *websocket_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
    return "/websockets.html";
}

char *index_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
    return "/newindex.ssi";
}

int generate_data(char *buf, int buflen)
{
    int uptime = xTaskGetTickCount() * portTICK_PERIOD_MS / 1000;
    int heap = (int) xPortGetFreeHeapSize();
    float temperature = -1;//0.0/0.0;
    float humidity = -1;//0.0/0.0;
    time_t ambient_last_updated = 0;
    
    if (xSemaphoreTake(ambientSemaphore,portTICK_PERIOD_MS*1000*2) == pdTRUE) {
        temperature = (float)ambient.temperature/10;
        humidity = (float)ambient.humidity/10;
        ambient_last_updated = ambient.last_updated;
        xSemaphoreGive(ambientSemaphore);
    }
    
    
    /* Generate response in JSON format */
    return snprintf(buf, buflen,
                    "{\"uptime\" : \"%d\","
                    " \"heap\" : \"%d\","
                    " \"temperature\" : \"%.2f\","
                    " \"humidity\" : \"%.2f\","
                    " \"ambient_last_updated\" : \"%u\"}",
                    uptime, heap, temperature, humidity,(uint32_t)ambient_last_updated);
}

void websocket_task(void *pvParameter)
{
    struct tcp_pcb *pcb = (struct tcp_pcb *) pvParameter;
    time_t last_checked = (time_t)0;
    time_t last_updated = (time_t)0;
    printf("Processing new stream request\n");

    
    for (;;) {
        if (pcb == NULL || pcb->state != ESTABLISHED) {
            printf("Connection closed, deleting task\n");
            break;
        }

        last_updated = ambient.last_updated;
        if (last_updated != last_checked)
        {
            char response[256];
            int len = generate_data(response,sizeof(response));
            
            printf("%s\n",response);
            if (len < sizeof (response))
            {
                LOCK_TCPIP_CORE();
                websocket_write(pcb, (unsigned char *) response, len, WS_TEXT_MODE);
                UNLOCK_TCPIP_CORE();
            }
            last_checked = last_updated;
        }
        vTaskDelayMs(1*1000);
    }

    vTaskDelete(NULL);
}

/**
 * This function is called when websocket frame is received.
 *
 * Note: this function is executed on TCP thread and should return as soon
 * as possible.
 */
void websocket_cb(struct tcp_pcb *pcb, uint8_t *data, u16_t data_len, uint8_t mode)
{
    printf("[websocket_callback]:\n%.*s\n", (int) data_len, (char*) data);
}
    
/*void websocket_cb(struct tcp_pcb *pcb, uint8_t *data, u16_t data_len, uint8_t mode)
{
    printf("[websocket_callback]:\n%.*s\n", (int) data_len, (char*) data);

    uint8_t response[2];
    uint16_t val;

    switch (data[0]) {
        case 'A': // ADC
            val = sdk_system_adc_read();
            break;
        case 'D': // Disable LED
            gpio_write(LED_PIN, true);
            val = 0xDEAD;
            break;
        case 'E': // Enable LED
            gpio_write(LED_PIN, false);
            val = 0xBEEF;
            break;
        default:
            printf("Unknown command\n");
            val = 0;
            break;
    }

    response[1] = (uint8_t) val;
    response[0] = val >> 8;

    LOCK_TCPIP_CORE();
    websocket_write(pcb, response, 2, WS_BIN_MODE);
    UNLOCK_TCPIP_CORE();
}*/

/**
 * This function is called when new websocket is open and
 * creates a new websocket_task if requested URI equals '/stream'.
 */
void websocket_open_cb(struct tcp_pcb *pcb, const char *uri)
{
    printf("WS URI: %s\n", uri);
    if (!strcmp(uri, "/stream")) {
        printf("request for streaming\n");
        xTaskCreate(&websocket_task, "websocket_task", 512, (void *) pcb, 4, NULL);
    }
}

void httpd_task(void *pvParameters)
{
    printf("Starting HTTP daemon for real\n");
    tCGI pCGIs[] = {
        {"/live", (tCGIHandler) websocket_cgi_handler},
    };

    const char *pcConfigSSITags[] = {
        "uptime", // SSI_UPTIME
        "heap",   // SSI_FREE_HEAP
        "temp", // SSI_TEMPERATURE
        "humidity", // SSI_HUMIDITY
        "lst_updt"  // SSI_AMB_UPDATED
    };

    /* register handlers and start the server */
    http_set_cgi_handlers(pCGIs, sizeof (pCGIs) / sizeof (pCGIs[0]));
    http_set_ssi_handler((tSSIHandler) ssi_handler, pcConfigSSITags,
            sizeof (pcConfigSSITags) / sizeof (pcConfigSSITags[0]));
    websocket_register_callbacks((tWsOpenHandler) websocket_open_cb,
            (tWsHandler) websocket_cb);
    printf("Calling httpd_init\n");
    httpd_init();

/*    printf("Loopping\n");
      for (;;);*/
    vTaskDelete(NULL);
}

void sntp_task(void *pvParameters)
{
    char *servers[] = {SNTP_SERVERS};
    UNUSED_ARG(pvParameters);
    
    time_t ts = time(NULL);
    printf("TIME: %s", ctime(&ts));

    /* wait for a IP */
    waitForIP();
    
    /* Start SNTP */
    printf("Starting SNTP... ");
    /* SNTP will request an update each 5 minutes */
    sntp_set_update_delay(5*60000);
    /* Set UTC zone, daylight savings off we keep hardware clock on UTC */
    const struct timezone tz = {0, 0};
    /* SNTP initialization */
    sntp_initialize(&tz);
    /* Servers must be configured right after initialization */
    sntp_set_servers(servers, sizeof(servers) / sizeof(char*));
    printf("SNTP DONE!\n");

    vTaskDelete(NULL);
}

void print_time_task(void *pvParameters)
{
    UNUSED_ARG(pvParameters);
    
    /* Print date and time each 5 seconds */
    while(1) {
        vTaskDelayMs(20*1000);
        time_t ts = time(NULL);
        printf("LOCAL TIME: %s", ctime(&ts));
        printf("UTC TIME: %s", asctime(gmtime(&ts)));
    }
}


void user_init(void)
{
    uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };
    
    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&config);
    sdk_wifi_station_connect();

    /* Initialize ambient variables */
    ambient.temperature = 0;
    ambient.humidity = 0;
    ambient.last_updated = 0;
    ambientSemaphore = xSemaphoreCreateMutex();

    if (!ambientSemaphore) {
        printf ("Unable to allocate semaphore, all is going bad\n");
    }

    /* Set the timezone */
    setenv("TZ","CET-1CEST,M3.5.0,M10.5.0/3",1);
    tzset();

    /* Start DHT task */
    printf ("Starting DHT task\n");
    xTaskCreate(&dhtMeasurementTask, "DHT task", 256, NULL, 10, NULL);
    
    /* wait for a IP */
/*    while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP) {
        printf("wifi status %d\n",sdk_wifi_station_get_connect_status());
        vTaskDelayMs(10000);
        }*/

    
    /* initialize network tasks */
    printf("Starting HTTP daemon\n");
    xTaskCreate(&httpd_task, "HTTP_Daemon", 8*1024, NULL, 5, NULL);
    printf("Starting SNTP daemon\n");
    xTaskCreate(&sntp_task, "SNTP Daemon", 1025, NULL, 3, NULL);
}
